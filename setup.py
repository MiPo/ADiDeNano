#!/usr/bin/env python3

from setuptools import setup, find_packages

import atmos_dispersion as my_pkg

setup(name='atmos_dispersion',
      author=my_pkg.__author__,
      author_email=my_pkg.__author_email__,
      classifiers=[
          'License :: OSI Approved :: Boost Software License 1.0',
          'Programming Language :: Python :: 3.7',
          'Topic :: Aerosol Physics :: Dispersion',
      ],
      description='Atmospheric dispersion of aerosol nanoparticles.',
      install_requires=['numpy', 'scipy', 'matplotlib'],
      keywords='Point source, Gaussian dispersion, Deposition',
      license=my_pkg.__license__,
      packages=find_packages(),
      test_suite='nose.collector',
      tests_require=['nose'],
      url='https://gitlab.com/MiPo/ADiDeNano',
      version=my_pkg.__version__,
      zip_safe=True)
