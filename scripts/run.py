#!/usr/bin/env python3

"""
Automated run of the dispersion model to calculate and save the
concentration field and deposition characteristics, uses inputs.py
to initialize values given for the model
"""

import numpy as np
from datetime import datetime
import time
import os
import atmos_dispersion.gaussiandispersion as agd
import inputs

########################################################################################################################
#  START TIMING  #
##################
# tic
t = time.time()
print('Program started.')

########################################################################################################################
#  DEFINE INPUT VARIABLES  #
############################
print('Initializing calculation.')

gridSize, Q, densityVect, dpVect, depoSwitch, vSet, vDep, UVect, TempVect, paramOptionVect, HpblVect, \
        stabClassVect, xMax, yMax, HVect, g, k = inputs.inputDefault()

########################################################################################################################
#  INITIALIZE PARAMETER VECTORS  #
##################################
# Sizes of the input arrays
sizeHpbl = np.shape(HpblVect)[0]
sizeU = np.shape(UVect)[0]
sizeTemp = np.shape(TempVect)[0]
sizeParamOption = np.shape(paramOptionVect)[0]
sizeH = np.shape(HVect)[0]
sizeDensity = np.shape(densityVect)[0]
if depoSwitch:
    vSetVect = vSet
    vDepVect = vDep
    sizeVset = np.shape(vSetVect)[0]
    sizeVdep = np.shape(vDepVect)[0]
else:
    vSetVect = [0.0]
    vDepVect = [0.0]
    sizeVset = 1
    sizeVdep = 1

zdist = np.zeros(shape=(sizeHpbl, gridSize), dtype=float)

# Distance vectors either in logarithmic 'Log' or linear range 'Lin'
distInput = 'Log'  # Switch
if distInput == 'Log':
    xdist = np.logspace(1, np.log10(xMax), gridSize)  # start distance 10 m
    ydist = np.logspace(-2, np.log10(yMax), gridSize)  # start distance 1 cm
    for idHpbl, Hpbl in enumerate(HpblVect):
        zdist[idHpbl, :] = np.logspace(-2, np.log10(HpblVect[idHpbl] - 0.1), gridSize)  # start distance 1 cm
elif distInput == 'Lin':
    xdist = np.linspace(10.0, xMax, gridSize)  # start distance 10 m
    ydist = np.linspace(0.01, yMax, gridSize)  # start distance 1 cm
    for idHpbl, Hpbl in enumerate(HpblVect):
        zdist[idHpbl, :] = np.linspace(0.01, HpblVect[idHpbl] - 0.1, gridSize)  # start distance 1 cm
else:
    raise Exception(
        'The chosen distance vector type is not supported. Please select Log or Lin.')

# Particle diameter vector
dpSwitch = 'Constant'  # Predefined particle sizes 'Constant' or a logarithmic distribution of different values 'Log'
if dpSwitch == 'Constant':
    sizeDp = np.shape(dpVect)[0]
else:
    sizeDp = 1
    dpVect = np.logspace(np.log10(10e-9), np.log10(500e-9), sizeDp)

# Concentration matrix
Ccg = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)
Cer = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)
Cya = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)
Cra = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)
Cwm = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)
Cmb = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                      sizeVdep, sizeDensity, gridSize, gridSize, gridSize), dtype=float)

########################################################################################################################
#  CALCULATE CONCENTRATIONS  #
##############################
print('Calculating concentrations.')
# Total number of model runs
totRuns = sizeDp * len(stabClassVect) * sizeHpbl * sizeU * sizeTemp * sizeParamOption * sizeH * sizeVdep * sizeDensity
run = 1
# Concentrations at coordinates (xi, yi, zi) for different particle size and stability classes
for idStab, stabClass in enumerate(stabClassVect):
    for idDp, dp in enumerate(dpVect):
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    vSet = vSetVect[idvDep]  # settling velocity coupled to deposition velocity
                                    print('run no.', int(run), ' / ', int(totRuns))
                                    if depoSwitch:
                                        # noinspection PyUnboundLocalVariable
                                        Ccg[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cer[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cya[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cra[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cwm[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :] = \
                                            agd.plumeConc(Q, U, xdist, ydist, zdist[idHpbl, :], H, Hpbl, dp, Temp,
                                                          density, g, k, paramOption, stabClass, vSet=vSet, vDep=vDep)
                                    else:
                                        Ccg[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cer[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cya[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cra[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                            Cwm[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :] = \
                                            agd.plumeConc(Q, U, xdist, ydist, zdist[idHpbl, :], H, Hpbl, dp, Temp,
                                                          density, g, k, paramOption, stabClass)
                                    run += 1

print('Concentrations calculated.')

# Print the end concentration value
print('The concentration at the distance of', "{:.0f}".format(
    xdist[-1]), 'meters is', "{:.9f}".format(Cra[-1, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0] * 1e6), 'µg/m3.')

########################################################################################################################
#  CALCULATE NUMERICAL DEPOSITION RATE  #
#########################################
print('Calculating deposition.')
# Initialize the variables
xGridSize = np.append(xdist[0], np.diff(xdist))
yGridSize = np.append(ydist[0], np.diff(ydist))
xGridSize2D, yGridSize2D = np.meshgrid(xGridSize, yGridSize)
xGridSize2D = np.transpose(xGridSize2D)
yGridSize2D = np.transpose(yGridSize2D)
Fdep = 0.0
Mdep = 0.0
MdepX = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                        sizeVdep, sizeDensity, gridSize), dtype=float)
MdepXcum = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                           sizeVdep, sizeDensity, gridSize), dtype=float)
vDepRannik = np.zeros(shape=(sizeDp, sizeTemp, sizeU, sizeDensity), dtype=float)
vSetRannik = np.zeros(shape=(sizeDp, sizeTemp, sizeDensity), dtype=float)

# Calculate numerical deposition rate for each stability class and particle size
for idStab, stabClass in enumerate(stabClassVect):
    for idDp, dp in enumerate(dpVect):
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    vSet = vSetVect[idvDep]  # settling velocity coupled to deposition velocity
                                    # Calculate deposition velocity
                                    if not depoSwitch:
                                        vDep = agd.depositionVelocity(density, k, g, dp, Temp, U)  # m/s
                                        vSet = agd.settlingVelocity(dp, density, g, Temp)  # m/s
                                        vDepRannik[idDp, idT, idU, idDen] = vDep
                                        vSetRannik[idDp, idT, idDen] = vSet
                                    # Calculate deposition flux and rate
                                    Fdep, Mdep, \
                                        MdepX[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :], \
                                        MdepXcum[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :] = \
                                        agd.depositionRate(xdist, ydist, zdist[idHpbl, :], vDep,
                                                           Cra[idStab, idDp, idHpbl, idU, idT, idOp, idH,
                                                           idvDep, idDen, :, :, :])

# Print total area
print('Total area is', "{:.0f}".format(np.sum(xGridSize2D * yGridSize2D)/1e6), 'km2 reference', "{:.0f}".format(
    xMax/1e3*yMax/1e3), 'km2')

# Test/compare the calculation of the integral for the last calculated case
print('Compare the numerical total deposition rate, reference (Mdep)', "{:.3f}".format(np.sum(2.0 * Mdep)), 'MdepXcum',
      "{:.3f}".format(MdepXcum[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]), 'g/s.')

########################################################################################################################
#  CALCULATE ANALYTICAL DEPOSITION RATE  #
##########################################
# Total deposition rate prior to distance x
# Initialize deposition rate matrix
MdepAnal = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                           sizeVdep, sizeDensity, gridSize), dtype=float)
for idStab, stab in enumerate(stabClassVect):
    for idDp, dp in enumerate(dpVect):
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    vSet = vSetVect[idvDep]  # settling velocity coupled to deposition velocity
                                    if depoSwitch:
                                        MdepAnal[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :] = \
                                            agd.depoRateRao(Q, U, xdist, H, dp, Temp, density, g, k, paramOption, stab,
                                                            vSet=vSet, vDep=vDep)
                                    else:
                                        MdepAnal[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :] = \
                                            agd.depoRateRao(Q, U, xdist, H, dp, Temp, density, g, k, paramOption, stab)

# Print the end deposition rate values
print('The numerical total deposition rate in the area of', "{:.0f}".format(
    xMax/1e3*yMax/1e3), 'km2 is', "{:.3f}".format(2.0 * np.sum(Mdep)), 'g/s.')

print('The analytical total deposition rate prior to distance of', "{:.0f}".format(xMax/1e3), 'km is',
      "{:.3f}".format(MdepAnal[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]), 'g/s.')

print('Deposition calculated.')

########################################################################################################################
#  CALCULATE SUSPENSION RATE  #
###############################
print('Calculating the amount aerosols suspended in air.')
# Initialize the variables
Fsusp = 0.0
Msusp = 0.0
MsuspX = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                         sizeVdep, sizeDensity, gridSize), dtype=float)

# Calculate numerical suspension rate for each stability class and particle size
for idStab, stabClass in enumerate(stabClassVect):
    for idDp, dp in enumerate(dpVect):
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    # Suspension rate for each distance x
                                    Fsusp, Msusp, \
                                        MsuspX[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :] = \
                                        agd.suspensionRate(xdist, ydist, zdist[idHpbl, :], U,
                                                           Cra[idStab, idDp, idHpbl, idU, idT, idOp, idH,
                                                           idvDep, idDen, :, :, :])

print('The suspension rate at distance of', "{:.0f}".format(xMax/1e3), 'km is',
      "{:.3f}".format(MsuspX[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]), 'g/s.')

print('Suspension rate calculated.')

########################################################################################################################
#  MASS BALANCE CORRECTION  #
#############################
# Corrected concentration field, deposition ratio and suspension ratio
print('Calculating mass balance correction.')
# Initialize the variables
numIter = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                          sizeVdep, sizeDensity), dtype=int)
tolerance = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                            sizeVdep, sizeDensity), dtype=float)
MdepXcumCorr = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                               sizeVdep, sizeDensity, 200, gridSize), dtype=float)
MsuspXcorr = np.zeros(shape=(np.shape(stabClassVect)[0], sizeDp, sizeHpbl, sizeU, sizeTemp, sizeParamOption, sizeH,
                             sizeVdep, sizeDensity, 200, gridSize), dtype=float)
# Calculate corrected concentration field for each stability class and particle size
run = 1
for idStab, stabClass in enumerate(stabClassVect):
    for idDp, dp in enumerate(dpVect):
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    print('Mass balance correction no.', int(run), ' / ', int(totRuns))
                                    # Calculate deposition velocity
                                    if not depoSwitch:
                                        vDep = agd.depositionVelocity(density, k, g, dp, Temp, U)  # m/s
                                    # Calculate corrected concentration, deposition and suspension
                                    Cmb[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :, :], \
                                        numIter[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen], \
                                        tolerance[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen], \
                                        MdepXcumCorr[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :], \
                                        MsuspXcorr[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :, :] = \
                                        agd.massBalanceIteration(Q, xdist, ydist, zdist[idHpbl, :], vDep, U,
                                                                 Cra[idStab, idDp, idHpbl, idU, idT, idOp, idH,
                                                                 idvDep, idDen, :, :, :])
                                    run += 1

print('Mass balance corrected concentration calculated.')

########################################################################################################################
#  X50 DISTANCE  #
##################
print('Calculating x50 distances.')
# Distance before which 50 % of the deposition has happened
CumDepo = np.zeros(shape=(np.shape(stabClassVect)[0], np.shape(dpVect)[0], sizeHpbl, sizeU, sizeTemp, sizeParamOption,
                          sizeH, sizeVdep, sizeDensity, np.shape(xdist)[0]), dtype=float)
X50 = np.zeros(shape=(np.shape(stabClassVect)[0], np.shape(dpVect)[0], sizeHpbl, sizeU, sizeTemp, sizeParamOption,
                      sizeH, sizeVdep, sizeDensity), dtype=float)
for idStab, stab in enumerate(stabClassVect):  # index 2 == class c # [2,0,:]
    for idDp, dp in enumerate(dpVect):  # index 0 == 10 nm
        for idHpbl, Hpbl in enumerate(HpblVect):
            for idU, U in enumerate(UVect):
                for idT, Temp in enumerate(TempVect):
                    for idOp, paramOption in enumerate(paramOptionVect):
                        for idH, H in enumerate(HVect):
                            for idvDep, vDep in enumerate(vDepVect):
                                for idDen, density in enumerate(densityVect):
                                    # Normalize with source strength
                                    idIter = numIter[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen] - 1
                                    CumDepo[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen, :] = \
                                        MdepXcumCorr[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen,
                                                     idIter, :] / Q
                                    # Find closest value to 0.5 in the deposition vector
                                    indexX50 = (np.abs(CumDepo[idStab, idDp, idHpbl, idU, idT, idOp, idH,
                                                       idvDep, idDen, :] - 0.5)).argmin()
                                    # Find distance
                                    X50[idStab, idDp, idHpbl, idU, idT, idOp, idH, idvDep, idDen] = \
                                        xdist[indexX50] / 1000

# Print min and max of x50
print(r'The $x_{50}$ distance minimum is ', "{:.3f}".format(np.min(X50)), ' km and maximum is ',
      "{:.3f}".format(np.max(X50)), 'km.')

print('x50 distances calculated.')

########################################################################################################################
#  CALCULATE DISPERSION PARAMETERS  #
#####################################
print('Calculating dispersion parameters.')
sigY = np.zeros(shape=(gridSize, np.shape(stabClassVect)[0], sizeParamOption), dtype=float)
sigZ = np.zeros(shape=(gridSize, np.shape(stabClassVect)[0], sizeParamOption), dtype=float)
# Dispersion parameters as function of x and stab class
for idStab, stab in enumerate(stabClassVect):
    for idParamOption, paramOption in enumerate(paramOptionVect):
        sigY[:, idStab, idParamOption], sigZ[:, idStab, idParamOption] = agd.dispersionParams(xdist, paramOption, stab)

print('Dispersion parameters calculated.')

########################################################################################################################
#  SAVE DATA  #
###############
saving = 'True'  # Switch, Save ON = 'True', Save OFF = 'False'
fileExt = 'npz'  # Choose data save file extension: 'npz'
if saving:
    print('Saving data.')
    # Create a directory with current time
    today = datetime.now()
    folder = today.strftime('%Y%m%d%H%M')
    os.mkdir("../data/" + folder)
    if fileExt == 'npz':
        filename = '../data/' + folder + '/results.npz'
        np.savez(filename, Q=Q, UVect=UVect, xdist=xdist, ydist=ydist, zdist=zdist, HVect=HVect, TempVect=TempVect,
                 densityVect=densityVect, g=g, k=k, paramOptionVect=paramOptionVect,
                 Ccg=Ccg, Cer=Cer, Cya=Cya, Cra=Cra, Cwm=Cwm, Cmb=Cmb, vDepVect=vDepVect, vSetVect=vSetVect,
                 Fdep=Fdep, Mdep=Mdep, MdepAnal=MdepAnal, dpVect=dpVect, stabClassVect=stabClassVect, HpblVect=HpblVect,
                 sigY=sigY, sigZ=sigZ,
                 MdepX=MdepX, MdepXcum=MdepXcum, xGridSize2D=xGridSize2D, yGridSize2D=yGridSize2D, MsuspX=MsuspX,
                 numIter=numIter, tolerance=tolerance, MdepXcumCorr=MdepXcumCorr, MsuspXcorr=MsuspXcorr, X50=X50,
                 vDepRannik=vDepRannik, vSetRannik=vSetRannik)
    else:
        raise Exception(
            'The chosen file extension is not supported. Please select npz.')
    print('Data saved to folder ' + folder + '.')

########################################################################################################################
#  END PROGRAM  #
#################
# toc
elapsed = time.time() - t
print('Program ended. Time elapsed', "{:.0f}".format(elapsed / 60), 'min.')
