def inputDefault():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    #   - SB4N TiO2 298 tons/yr -> 9.45 g/s
    #   - Fonseca paint, Batch TiO2 max 0.51 g/h -> 1.4e-4 g/s, avg 0.9kg/year -> 2.9e-5 g/s
    #   - Koivisto TiO2 max 20mg/min -> 3.3e-4 g/s, avg. 110g/year -> 3.5e-6 g/s
    Q = 2.9e-5  # source rate (g/s)

    # Source height
    H = [10.0]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [1e-4]  # settling velocity (m/s)
    vDep = [1e-3]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [100e-9]  # particle diameter (m), input range 10 - 500 nm
    density = [4230.0]  # particle density (kg/m3) # TiO2
    Temp = [288.15]  # [243.15, 303.15]  # air temperature K
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [2.5]  # wind speed (m/s)
    paramOption = ['davidson']  # dispersion parametrization
    Hpbl = [1000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['d']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputGeneric():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 2.9e-5  # Fonseca paint avg 0.9kg/year -> 2.9e-5 g/s,

    # Source height
    H = [2.0, 10.0, 50.0]  # source height m

    # Deposition velocity
    depoSwitch = True
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [1e-6, 1e-4, 1e-3]  # settling velocity (m/s)
    vDep = [1e-4, 1e-3, 1e-2]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [10e-9]  # particle diameter (m), input range 10 - 500 nm
    density = [1.0]  # particle density (kg/m3) # TiO2
    Temp = [1.0]  # [243.15, 303.15]  # air temperature K
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [1.0, 2.5, 10.0]  # wind speed (m/s)
    paramOption = ['davidson', 'klug']  # dispersion parametrization
    Hpbl = [200.1, 1000.1, 2000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'd', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputNano():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 2.9e-5  # Fonseca paint avg 0.9kg/year -> 2.9e-5 g/s

    # Source height
    H = [2.0, 50.0]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [0.0]  # settling velocity (m/s)
    vDep = [0.0]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [10e-9, 100e-9, 500e-9]  # particle diameter (m), input range 10 - 500 nm
    density = [1000.0, 4230.0, 18000.0]  # particle density (kg/m3) # TiO2 bulk density
    Temp = [243.15, 273.15, 303.15]  # air temperature K, [-30, 30] in C
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [1.0, 10.0]  # wind speed (m/s)
    paramOption = ['davidson', 'klug']  # dispersion parametrization
    Hpbl = [200.1, 2000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'd', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputCaseFonseca():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 2.9e-5  # Fonseca paint avg 0.9kg/year -> 2.9e-5 g/s

    # Source height
    H = [7.8]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [0.0]  # settling velocity (m/s)
    vDep = [0.0]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [260e-9]  # particle diameter (m), input range 10 - 500 nm
    density = [940.0]  # particle density (kg/m3) # TiO2 pigment bulk density
    Temp = [288.15]  # air temperature K, 15C
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [2.5]  # wind speed (m/s)
    paramOption = ['davidson']  # dispersion parametrization
    Hpbl = [1000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'b', 'c', 'd', 'e', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputCaseFonsecaProcess():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 0.14e-3  # Fonseca paint, Batch TiO2 max 0.51 g/h -> 1.4e-4 g/s

    # Source height
    H = [7.8]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [0.0]  # settling velocity (m/s)
    vDep = [0.0]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [260e-9]  # particle diameter (m), input range 10 - 500 nm
    density = [940.0]  # particle density (kg/m3) # TiO2 pigment bulk density
    Temp = [288.15]  # air temperature K, 15C
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [2.5]  # wind speed (m/s)
    paramOption = ['davidson']  # dispersion parametrization
    Hpbl = [1000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'b', 'c', 'd', 'e', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputCaseKoivisto():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 3.5e-6  # Koivisto TiO2 avg. 110g/year -> 3.5e-6 g/s

    # Source height
    H = [3.0]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [0.0]  # settling velocity (m/s)
    vDep = [0.0]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [280e-9]  # particle diameter (m), Far-field TiO2-N number size distribution mode
    density = [2100.0]  # particle density (kg/m3) # TiO2 effective density
    Temp = [288.15]  # air temperature K, 15C
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [2.5]  # wind speed (m/s)
    paramOption = ['davidson']  # dispersion parametrization
    Hpbl = [1000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'b', 'c', 'd', 'e', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k


def inputCaseKoivistoProcess():
    # Number of points to be calculated in the each direction x, y, z (defines the spatial resolution)
    gridSize = 100  # Use initial grid size 20-100, and final grid size 100-400 # Use values that are divisible by two

    # Particle source rate (g/s)
    Q = 0.33e-3  # Koivisto TiO2 max 20mg/min -> 3.3e-4 g/s

    # Source height
    H = [3.0]  # source height m

    # Deposition velocity
    depoSwitch = False
    # OPTION 1: Give deposition and settling velocities as input value, depoSwitch = True
    # vSet and vDep are coupled so that the first vSet value corresponds to first vDep value
    vSet = [0.0]  # settling velocity (m/s)
    vDep = [0.0]  # deposition velocity (m/s), be sure that vDep > vSet, and vDep < 1e-2
    # OPTION 2: Calculate nanoparticle deposition using dp, density, Temp, g and k, depoSwitch = False
    dp = [280e-9]  # particle diameter (m), Far-field TiO2-N number size distribution mode
    density = [2100.0]  # particle density (kg/m3) # TiO2 effective density
    Temp = [288.15]  # air temperature K, 15C
    g = 9.81  # gravitational acceleration (m/s2)
    k = 1.38064852e-23  # Boltzmann constant m2*kg/(s2*K)

    # Environmental parameters
    U = [2.5]  # wind speed (m/s)
    paramOption = ['davidson']  # dispersion parametrization
    Hpbl = [1000.1]  # m, height of the planetary boundary layer > 200 m
    stabClass = ['a', 'b', 'c', 'd', 'e', 'f']  # atmospheric stability class a - f

    # Location parameters, maximum distances in each direction
    xMax = 500 * 1e3  # m < 500 km
    yMax = 500 * 1e3  # m < 500 km, usually yMax = 0.1 * xMax is sufficient

    return gridSize, Q, density, dp, depoSwitch, vSet, vDep, U, Temp, paramOption, Hpbl, \
        stabClass, xMax, yMax, H, g, k
