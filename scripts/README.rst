ADiDeNano - Scripts
===================

inputs.py -- initialise the simulation input values

run.py -- run the simulation and save the results

plot.py -- read the results file and plot the data
