#!/usr/bin/env python3

"""
Unit tests for the Gaussian dispersion and the functions used for
calculation. In progress... Created for an older version of the code.
Not tested for the current version 1.0 of the code.
"""

import unittest
from atmos_dispersion.gaussiandispersion import (
    plumeConc, settlingVelocity, depositionVelocity, schmidt,
    diffusivity, viscosity, cunningham, airMeanPath, dispersionParams,
    davidson, klug)


class test_dispersion(unittest.TestCase):
    """
    Unit testing for different values for the functions as well as
    incorrect input values. Checks that the functions return a value and
    that the value is almost equal to a testing value.
    """

    # Define input values, two first values are own testing values, third,
    # fourth, fifth and sixth values are from Stockie (2011) for
    # comparison purpose
    Q = [1e-4, 1e-4, 1.1, 1.1, 1.1, 1.1]
    dp = [100e-9, 100e-9, 0.5e-6, 0.5e-6, 0.5e-6, 0.5e-6]
    density = [1000.0, 1000.0, 3500.0, 3500.0, 3500.0, 3500.0]
    U = [2.6, 2.6, 5.0, 5.0, 5.0, 5.0]
    Temp = [300.0, 300.0, 300.0, 300.0, 300.0, 300.0]
    paramOption = ['davidson', 'klug', 'klug', 'klug', 'klug', 'klug']
    stabClass = ['c', 'c', 'c', 'c', 'c', 'c']
    x = [200.0, 200.0, 1500.0, 200.0, 303.0, 1700.0]
    y = [20.0, 20.0, 0.0, 0.0, 0.0, 260.0]
    z = [20.0, 20.0, 0.0, 0.0, 0.0, 0.0]
    H = [10.0, 10.0, 15.0, 15.0, 15.0, 15.0]
    Hpbl = [1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0]
    g = [9.81, 9.81, 9.8, 9.8, 9.8, 9.8]
    k = [1.38064852e-23]

    def test_plumeConc(self):
        """
        Test the function plumeConc with multiple values
        """
        outputs = [1.13e-8, 1.06e-8, 0.01e-3,
                   0.1e-3, 0.1001e-3, 0.001e-3]  # g/m3
        places = [10, 10, 5, 4, 4, 6]
        for idOut, output in enumerate(outputs):
            Conc = plumeConc(self.Q[idOut], self.U[idOut],
                             self.x[idOut], self.y[idOut], self.z[idOut],
                             self.H[idOut], self.Hpbl[idOut], self.dp[idOut],
                             self.Temp[idOut], self.density[idOut],
                             self.g[idOut], self.k[0], self.paramOption
                             [idOut], self.stabClass[idOut])

            self.assertFalse(isinstance(Conc, type(None)))
            self.assertAlmostEqual(
                Conc, outputs[idOut], places=places[idOut])  # g/m3

    def test_settlingVelocity(self):
        """
        Test the function settlingVelocity
        """
        vSet = settlingVelocity(
            self.dp[0], self.density[0], self.g[0], self.Temp[0])
        self.assertFalse(isinstance(vSet, type(None)))
        self.assertAlmostEqual(vSet, 8.7e-7)  # m/s

    def test_depositionVelocity(self):
        """
        Test the function depositionVelocity
        """
        vDep = depositionVelocity(
            self.density[0], self.k[0], self.g[0], self.dp[0],
            self.Temp[0], self.U[0])
        self.assertFalse(isinstance(vDep, type(None)))
        self.assertAlmostEqual(vDep, 0.04e-2, places=4)  # m/s

    def test_schmidt(self):
        """
        Test the function schmidt
        """
        Sc = schmidt(self.k[0], self.Temp[0], self.dp[0])
        self.assertFalse(isinstance(Sc, type(None)))
        self.assertAlmostEqual(Sc, 22443, places=0)  # unitless

    def test_diffusivity(self):
        """
        Test the function diffusivity
        """
        D = diffusivity(self.k[0], self.Temp[0], self.dp[0])
        self.assertFalse(isinstance(D, type(None)))
        self.assertAlmostEqual(D, 7e-10)  # m2/s

    def test_viscosity(self):
        """
        Test the function viscosity
        """
        visc = viscosity(self.Temp[0])
        self.assertFalse(isinstance(visc, type(None)))
        self.assertAlmostEqual(visc, 1.846e-5)  # kg/m s

    def test_cunningham(self):
        """
        Test the function cunningham
        """
        Cc = cunningham(self.dp[0], self.Temp[0])
        self.assertFalse(isinstance(Cc, type(None)))
        self.assertAlmostEqual(Cc, 2.95, places=2)  # unitless

    def test_airMeanPath(self):
        """
        Test the function airMeanPath
        """
        path = airMeanPath(self.Temp[0])
        self.assertFalse(isinstance(path, type(None)))
        self.assertAlmostEqual(path, 68e-9)  # meters

    def test_dispersionParams(self):
        """
        Test the function dispersionParams
        """
        outY = [23.5, 21.3]  # m
        outZ = [14.0, 8.0]  # m
        for idOut, output in enumerate(outY):
            dispY, dispZ = dispersionParams(
                self.x[idOut], self.paramOption[idOut], self.stabClass[idOut])
            self.assertFalse(isinstance(dispY, type(None)))
            self.assertFalse(isinstance(dispZ, type(None)))
            self.assertAlmostEqual(dispY, outY[idOut], places=1)  # m
            self.assertAlmostEqual(dispZ, outZ[idOut], places=1)  # m

    def test_davidson(self):
        """
        Test the function davidson
        """
        davidY, davidZ = davidson(self.x[0], self.stabClass[0])
        self.assertFalse(isinstance(davidY, type(None)))
        self.assertFalse(isinstance(davidZ, type(None)))
        self.assertAlmostEqual(davidY, 23.5, places=1)  # m
        self.assertAlmostEqual(davidZ, 14.0, places=1)  # m

    def test_klug(self):
        """
        Test the function klug
        """
        klgY, klgZ = klug(self.x[0], self.stabClass[1])
        self.assertFalse(isinstance(klgY, type(None)))
        self.assertFalse(isinstance(klgZ, type(None)))
        self.assertAlmostEqual(klgY, 21.3, places=1)  # m
        self.assertAlmostEqual(klgZ, 8.0, places=1)  # m

    def test_incorrectInputs(self):
        """
        Test the for cases of incorrect values for the input parameters
        """
        with self.assertRaises((AssertionError, TypeError, Exception)):
            airMeanPath(0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            airMeanPath('a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            viscosity(0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            viscosity('a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            cunningham(0, 100e-9)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            cunningham('a', 100e-9)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            cunningham(300, 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            cunningham(300, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            settlingVelocity('a', 0, 0, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            diffusivity('a', 0, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            schmidt('a', 0, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            depositionVelocity(1, 1, 'a', 'a', 300, 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            depositionVelocity(1, 1, 9.8, 9e-9, 300, 5)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            depositionVelocity(1, 1, 9.8, 2.5e-6, 300, 5)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            davidson(10, 'tre')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            davidson(10, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            davidson(10, 'g')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            klug(10, 'tre')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            klug(10, 0)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            klug(10, 'g')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            dispersionParams(10, 'fail', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            dispersionParams(10, 0, 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc('a', 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 'a', 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 'a', 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 'a', 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 'a', 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 'a', 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 'a', 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 'a',
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      'a', 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 'a', 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 'a', 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 10, 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 10)
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(0, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 0, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 0, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, -1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, -10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, -100e-9, 300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, -300,
                      1000, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      0, 9.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 7.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 10.81, 1.38e-23, 'klug', 'a')
        with self.assertRaises((AssertionError, TypeError, Exception)):
            plumeConc(1, 1, 1, 1, 1, 10.0, 1000.0, 100e-9, 300,
                      1000, 9.81, -1.38e-23, 'klug', 'a')
