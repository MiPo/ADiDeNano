"""
The package contains methods for modeling atmospheric dispersion of aerosol
nanoparticles from a point source.
"""

__author__ = "Mikko Poikkimäki"
__copyright__ = "Copyright 2022, Mikko Poikkimäki"
__author_email__ = "mikko.poikkimaki@ttl.fi"
__license__ = "Boost Software License 1.0"
__version__ = "1.0"

import atmos_dispersion.gaussiandispersion
