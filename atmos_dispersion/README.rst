ADiDeNano -- Package atmos_dispersion
============================================================

This package can be used for modeling atmospheric dispersion and deposition of aerosol nanoparticles originating from a point source.

Documentation of functions -- Package atmos_dispersion includes:

file gaussiandispersion.py
==========================
The Python code file gaussiandispersion.py is structured by using functions as follows:

plumeConc -- Calculates the aerosol concentration in atmosphere at a single location (x,y,z)
    settlingVelocity -- Calculates the settling velocity of particles.
    depositionVelocity -- Calculates the dry deposition velocity of particles.
        schmidt -- Calculates the Schmidt number.
        diffusivity -- Calculates the diffusivity of particles.
            viscosity -- Calculates the viscosity of air.
            cunningham -- Calculates the Cunningham slip correction factor for the particles.
                airMeanPath -- Calculates the mean free path of the particles in the air.
    dispersionParams -- Calculates the dispersion parameters.
        davidson -- Calculates the dispersion parameters using Davidson (1990).

        klug -- Calculates the dispersion parameters using Klug (1969).

    classicGaussian -- Calculates the aerosol concentration in air based on the classic
    Gaussian dispersion equation (plume).

    ermak -- Calculates the aerosol concentration in air based on Ermak (1977) solution of
    the Gaussian dispersion equation including particle settling and deposition.

    yamartino -- Calculates the aerosol concentration in air based on the Gaussian dispersion
    equation including the planetary boundary layer reflection in z-direction
    according to Yamartino, Jr. (1977) Journal of the Air Pollution Control Association.

    rao -- Calculates the aerosol concentration in air based on Rao (1981) eq. 17.
    Solution of the Gaussian dispersion equation including particle settling,
    deposition and boundary layer reflection.

    raoWellMixed -- Calculates the aerosol concentration in air based on Rao (1981) eq. 20.
    Solution of the Gaussian dispersion equation including particle settling,
    deposition and boundary layer reflection for well mixed region.

depoRateRao -- Calculates the deposition rate of particles to ground based on Ermak (1977) Eq. 8 and Rao (1981) Eq. 25a.

massBalanceIteration -- Calculates concentration corrected with mass balance, iterate until a tolerance of 1 percent is reached.

    depositionRate -- Calculates the deposition rate at downwind distance x and the total deposition before distance x.

    suspensionRate -- Calculates the suspension rate at downwind distance x meaning particles suspended in air.

    massBalance -- Calculates concentration corrected with mass balance.

folder tests
============
Including unit testing of the atmos_dispersion.gaussiandispersion implemented into file test_disp.py

