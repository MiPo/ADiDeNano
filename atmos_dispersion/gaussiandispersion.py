#!/usr/bin/env python3

"""
Implementation of the Gaussian dispersion equation for continuous point
source, 'Plume model', taking settling and deposition velocity of the
particles into account by employing the dry deposition parametrization by
Rannik et al. (2003). The dispersion parameters are calculated for the
Pasquill atmospheric stability classes by using parametrization of Klug
(1969) or Davidson (1990). The code calculates the concentration with
five different Gaussian solutions:
- Classic Gaussian dispersion
- Ermak (1977) including settling and deposition velocities,
- Yamartino (1977) including boundary layer reflection and
- Rao (1981) including both.
- Rao (1981) for a well mixed region
Additionally, a mass balance corrected concentration is calculated
from the solution of Rao.
"""

import numpy as np
import scipy.special as sp
from scipy import ndimage


def airMeanPath(Temp):
    """
    Calculates the mean free path of the particles in the air.

    Parameters
    ----------
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    path        float
                The mean free path in meters.
    """

    assert isinstance(Temp, float)
    assert Temp > 0

    # Calculate the mean free path
    path = 6.73e-8 * Temp * (1 + 110.4 / Temp) / (296 * 1.373)

    # Check that our return value is sane
    assert isinstance(path, float)
    assert not np.isnan(path)
    assert not np.isinf(path)

    return path


def viscosity(Temp):
    """
    Calculates the dynamic viscosity of air.

    Parameters
    ----------
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    visc        float
                The dynamic viscosity of air in kg/(m*s).
    """

    assert isinstance(Temp, float)
    assert Temp > 0

    # Calculate the viscosity
    visc = 1.832e-5 * 406.4 * Temp**1.5 / (5093 * (Temp + 110.4))

    # Check that our return value is sane
    assert isinstance(visc, float)
    assert not np.isnan(visc)
    assert not np.isinf(visc)

    return visc


def cunningham(dp, Temp):
    """
    Calculates the Cunningham slip correction factor for the particles
    based on Hinds (1999).

    Parameters
    ----------
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    Cc          float
                The Cunningham slip correction factor (unit less).
    """

    assert isinstance(dp, float)
    assert dp > 0

    # Calculate the slip correction factor
    Cc = 1 + airMeanPath(Temp) / dp * (2.514 + 0.8 *
                                       np.exp(-0.55 * dp / airMeanPath(Temp)))

    # Check that our return value is sane
    assert isinstance(Cc, float)
    assert not np.isnan(Cc)
    assert not np.isinf(Cc)

    return Cc


def settlingVelocity(dp, density, g, Temp):
    """
    Calculates the settling velocity of particles based on Hinds (1999).

    Parameters
    ----------
    g           float
                The gravitational acceleration in m/s2.
    density     float
                The density of the particles in kg/m3.
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    vSet        float
                The settling velocity of the particles in m/s.
    """

    # Calculate settling velocity, Hinds (1999)
    vSet = g * density * cunningham(dp, Temp) * dp**2 / (18 * viscosity(
        Temp))

    # Check that our return value is sane
    assert isinstance(vSet, float)
    assert not np.isnan(vSet)
    assert not np.isinf(vSet)

    return vSet


def diffusivity(k, Temp, dp):
    """
    Calculates the diffusivity of particles based on Hinds (1999) and
    Rannik et al. (2003).

    Parameters
    ----------
    k           float
                The Boltzmann constant in m2*kg/(s2*K).
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    D           float
                The diffusivity of the particles in m2/s.
    """

    # Calculate the diffusivity
    D = k * Temp * cunningham(dp, Temp) / (3 * np.pi * viscosity(Temp) * dp)

    # Check that our return value is sane
    assert isinstance(D, float)
    assert not np.isnan(D)
    assert not np.isinf(D)

    return D


def schmidt(k, Temp, dp):
    """
    Calculates the Schmidt number.

    Parameters
    ----------
    k           float
                The Boltzmann constant in m2*kg/(s2*K).
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.

    Returns
    -------
    Sc          float
                The Schmidt number (unit less).
    """

    # Calculate the density of air
    densityAir = 1.2929 * 273.15 / Temp  # kg/m3

    # Calculate the Schmidt number
    Sc = viscosity(Temp) / (densityAir * diffusivity(k, Temp, dp))

    # Check that our return value is sane
    assert isinstance(Sc, float)
    assert not np.isnan(Sc)
    assert not np.isinf(Sc)

    return Sc


def depositionVelocity(density, k, g, dp, Temp, U):
    """
    Calculates the dry deposition velocity of particles based on Rannik et
    al., J. Geophys. Res., (2003). Valid for diameter range of 10 to 500 nm.

    Parameters
    ----------
    density     float
                The density of the particles in kg/m3.
    k           float
                The Boltzmann constant in m2*kg/(s2*K).
    g           float
                The gravitational acceleration in m/s2.
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.
    U           float
                The wind speed in m/s.

    Returns
    -------
    vDep        float
                The deposition velocity of the particles in m/s.
    """

    assert isinstance(U, float)
    assert isinstance(dp, float)
    assert isinstance(Temp, float)
    assert isinstance(density, float)
    assert isinstance(g, float)
    assert isinstance(k, float)
    assert U > 0
    assert Temp > 0
    assert density > 0
    assert g > 9
    assert g <= 10
    assert k > 0

    # Check the diameter values, since the model works with limited size scale
    assert dp >= 10e-9
    assert dp <= 500e-9

    # Constants and Empirical values from Rannik et al. (2003)
    cvToCd = 1 / 3  # the ratio of viscous to total drag Slinn, (1982)
    vFric = 0.4  # median friction velocity in m/s
    windCanopy = 1.5  # wind speed at canopy height in m/s
    gamma = 1.9  # coefficient of exponential decrease of wind speed inside canopy

    # Fit parameters based on Hyytiala forest measurements from Rannik et
    # al. (2003)
    dExponent = 2.5  # Fig. 7, TRFE < 50 %
    collection = 2.9  # collection efficiency factor, Fig. 7
    dmin = 146e-9  # diameter of minimum in collection efficiency, Fig. 7

    # Aerodynamic resistance in s/m, eq. 4
    ra = (U - windCanopy) / vFric**2

    # Collection efficiency due to Brownian diffusion, eq. 10
    brownian = cvToCd * schmidt(k, Temp, dp)**(-2 / 3)

    # Calculate prefactor in eq. 13 for particle diameter dmin
    cunninghamDmin = cunningham(dmin, Temp)  # eq. 6
    schmidtDmin = schmidt(k, Temp, dmin)  # eq. 11
    derivCunningham = -2.514 * airMeanPath(Temp) / dmin**2 - (airMeanPath(
        Temp) / dmin + 0.55) * 0.8 / dmin * np.exp(-0.55 * dmin /
                                                   airMeanPath(Temp))
    # derivative of the eq. 6
    derivSchmidt = schmidtDmin * \
        (1 / dmin - 1 / cunninghamDmin * derivCunningham)  # eq. 15
    prefactor = 2 / 3 * cvToCd * \
        schmidtDmin**(-5 / 3) * derivSchmidt / \
        (dExponent * dmin**(dExponent - 1))  # eq. 14

    # Empirical collection efficiency of particles, eq. 13
    empirical = prefactor * (dp**dExponent)

    # Total collection efficiency of particles, eq. 9
    eps = np.sqrt(collection * (brownian + empirical))

    # Canopy resistance, eq. 7 of Rannik et al.
    rc = windCanopy / (vFric**2 * eps) * ((1 + eps * np.tanh(gamma * eps)) / (eps + np.tanh(gamma * eps)))

    # Calculate settling velocity
    vSet = settlingVelocity(dp, density, g, Temp)

    # Calculate the dry deposition velocity, eq. 3
    vDep = vSet + 1 / (ra + rc)

    # Check that our return value is sane
    assert isinstance(vDep, float)
    assert not np.isnan(vDep)
    assert not np.isinf(vDep)

    return vDep


def davidson(xGrid, stabClass):
    """
    Calculates the dispersion parameters using Davidson, J. Air & Waste
    Man. Assoc., (1990).

    Parameters
    ----------
    xGrid       numpy array of floats (n x n x n)
                The distance in x-direction in m.
    stabClass   string
                The Pasquill atmospheric stability class, options from
                extremely unstable 'a' to stable 'f'.

    Returns
    -------
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.
    """

    xx = xGrid / 1000  # switch to km

    # Select the coefficients based on the stability class
    if stabClass == 'a':
        y = [209.6,   0.8804, -0.006902]
        sigY = y[0] * (xx ** (y[1] + y[2] * np.log(xx)))  # unit in m
        # xx < 0.45
        z = [310.4,   1.773,   0.1879]
        sigZ = z[0] * (xx ** (z[1] + z[2] * np.log(xx)))  # unit in m
        # 0.45 <= xx
        z = [453.9,   2.117,   0.0]
        sigZ[0.45 <= xx] = z[0] * (xx[0.45 <= xx] ** (z[1] + z[2] * np.log(xx[0.45 <= xx])))  # unit in m
    else:
        if stabClass == 'b':
            y = [154.7,   0.8932, -0.006271]
            z = [109.8,   1.064,   0.01163]
        elif stabClass == 'c':
            y = [103.3,   0.9112, -0.004845]
            z = [61.14,   0.9147,  0]
        elif stabClass == 'd':
            y = [68.28,   0.9112, -0.004845]
            z = [30.38,   0.7309, -0.032]
        elif stabClass == 'e':
            y = [51.05,   0.9112, -0.004845]
            z = [21.14,   0.6802, -0.04522]
        elif stabClass == 'f':
            y = [33.96,   0.9112, -0.004845]
            z = [13.72,   0.6584, -0.05367]
        else:
            raise Exception('The selected stability class not found.',
                            'Select a, b, c, d, e or f.')

        # Calculate the dispersion coefficients
        sigY = y[0] * (xx**(y[1] + y[2] * np.log(xx)))  # unit in m
        sigZ = z[0] * (xx**(z[1] + z[2] * np.log(xx)))  # unit in m

        # Check that monotonically increasing, if not then replace the rest of the values with the max value
        if len(sigY.shape) == 1:
            dx = np.append(0.0, np.diff(sigZ))
        else:
            dx = np.append(0.0, np.diff(sigZ[:, 0, 0]))
        sigZ[dx < 0] = np.max(sigZ)

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in sigY.flatten())
    assert not (np.isnan(sigY)).any()
    assert not (np.isinf(sigY)).any()

    assert all(isinstance(i, float) for i in sigZ.flatten())
    assert not (np.isnan(sigZ)).any()
    assert not (np.isinf(sigZ)).any()

    return sigY, sigZ


def klug(xGrid, stabClass):
    """
    Calculates the dispersion parameters using Klug (1969). The values can
    be found in Table 18.3 (p. 866) of Seinfeld & Pandis, (2006).

    Parameters
    ----------
    xGrid       numpy array of floats (n x n x n)
                The distance in x-direction in m.
    stabClass   string
                The Pasquill atmospheric stability class, options from
                extremely unstable 'a' to stable 'f'.

    Returns
    -------
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.
    """

    # Select the coefficients based on the stability class
    if stabClass == 'a':
        y = [0.469,  0.903]
        z = [0.017,  1.380]
    elif stabClass == 'b':
        y = [0.306, 0.885]
        z = [0.072, 1.021]
    elif stabClass == 'c':
        y = [0.230, 0.855]
        z = [0.076, 0.879]
    elif stabClass == 'd':
        y = [0.219, 0.764]
        z = [0.140, 0.727]
    elif stabClass == 'e':
        y = [0.237, 0.691]
        z = [0.217, 0.610]
    elif stabClass == 'f':
        y = [0.273, 0.594]
        z = [0.262, 0.500]
    else:
        raise Exception('The selected stability class not found.',
                        'Select a, b, c, d, e or f.')

    # Calculate the dispersion coefficients
    sigY = y[0] * (xGrid**y[1])  # unit in m
    sigZ = z[0] * (xGrid**z[1])

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in sigY.flatten())
    assert not (np.isnan(sigY)).any()
    assert not (np.isinf(sigY)).any()

    assert all(isinstance(i, float) for i in sigZ.flatten())
    assert not (np.isnan(sigZ)).any()
    assert not (np.isinf(sigZ)).any()

    return sigY, sigZ


def dispersionParams(xGrid, paramOption, stabClass):
    """
    Calculates the dispersion parameters using Klug (1969) or Davidson
    (1990).

    Parameters
    ----------
    xGrid       numpy array of floats (n x n x n)
                The distance in x-direction in m.
    paramOption string
                The parametrization to be used for calculation of the dispersion, options: 'davidson' and 'klug'.
    stabClass   string
                The Pasquill atmospheric stability class, options from
                extremely unstable 'a' to stable 'f'.

    Returns
    -------
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters for each location (x, y, z).
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters for each location (x, y, z).
    """

    assert all(isinstance(i, float) for i in xGrid.flatten())
    assert (xGrid > 0).all()

    # Selection of the parametrization and calculation of the values
    if paramOption == 'davidson':
        sigY, sigZ = davidson(xGrid, stabClass)

    elif paramOption == 'klug':
        sigY, sigZ = klug(xGrid, stabClass)

    else:
        raise Exception('The dispersion parametrization not found.',
                        'Select davidson or klug.')

    # Limit the dispersion in z-direction to 5000 meters, as discussed by Davidson, and used in EPA models
    sigZ[sigZ > 5000.0] = 5000.0

    return sigY, sigZ


def depoRateRao(Q, U, x, H, dp, Temp, density, g, k, paramOption, stabClass, **kwargs):
    """
    Calculates the deposition rate of particles to ground based on Ermak
    (1977) Eq. 8 and Rao (1981) Eq. 25a.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    x           float or numpy array of floats (n x 1)
                The distance in x-direction in m.
    H           float
                The height of the emission in m.
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.
    density     float
                The density of the particles in kg/m3.
    g           float
                The gravitational acceleration in m/s2.
    k           float
                The Boltzmann constant in m2*kg/(s2*K).
    paramOption string
                The parametrization to be used for calculation of the parameters, options: 'davidson' and 'klug'.
    stabClass   string
                The Pasquill atmospheric stability class, options from
                extremely unstable 'a' to stable 'f'.

    Optional    (**kwargs)
    --------
    vSet        float
                The settling velocity of the particles in m/s.
    vDep        float
                The deposition velocity of the particles in m/s.

    Returns
    -------
    Ndep        float or numpy array of floats (n x 1)
                The total deposition rate to ground up to downwind
                distance of x in units of g/s or #/s depending on the
                input source Q units.
    """

    # Check for inputs types and values
    assert isinstance(Q, float)
    assert isinstance(U, float)
    assert all(isinstance(i, float) for i in x.flatten())
    assert isinstance(H, float)
    assert isinstance(dp, float)
    assert isinstance(Temp, float)
    assert isinstance(density, float)
    assert isinstance(g, float)
    assert isinstance(k, float)
    assert isinstance(paramOption, str)
    assert isinstance(stabClass, str)
    assert Q > 0
    assert U > 0
    assert (x > 0).all()
    assert H >= 0
    assert dp > 0
    assert Temp > 0
    assert density > 0
    assert g > 9
    assert g <= 10
    assert k > 0

    # Calculate settling velocity if not given as input parameter
    vSet = kwargs.get('vSet', settlingVelocity(dp, density, g, Temp))
    if vSet == 0.0:  # Avoid division with zero
        vSet = 1.0e-300 + 1.0e-310

    # Calculate deposition velocity if not given as input parameter
    vDep = kwargs.get('vDep', depositionVelocity(density, k, g, dp, Temp, U))
    if vDep == 0.0:  # Avoid division with zero
        vDep = 1.0e-300

    # Calculate dispersion parameters sigY and sigZ
    sigY, sigZ = dispersionParams(x, paramOption, stabClass)

    # Calculate the diffusivity in z-direction (LleweLyn, 1983; Stockie, 2011)
    Kz = U * sigZ**2 / (2 * x)

    # Calculate velocity variable (auxiliary), used in next calculation
    v0 = vDep - 0.5 * vSet

    # Calculate the deposition until downwind location x
    # Ermak, (1977), eq. 8, same as Rao (1981) eq. 25a
    Term1 = 0.5 * sp.erfc(vSet * sigZ / (2**(3/2) * Kz) - H / (np.sqrt(2) * sigZ))
    Term2a = np.zeros(shape=(np.shape(x)), dtype=float)
    Term2b = sp.erfc(vSet * sigZ / (2**(3/2) * Kz) + H / (np.sqrt(2) * sigZ))

    # Term2 simplifies if Term2b is zero (to avoid overflow in Term2a)
    zeroTerm2b = Term2b < 1e-300  # boolean matrix
    Term2a[zeroTerm2b] = 0.0
    Term2a[~zeroTerm2b] = vDep / (2 * (vDep - vSet)) * np.exp(vSet * H / Kz[~zeroTerm2b])

    # Avoid overflow if error function part is zero then whole Term3 is zero. Otherwise calculate normally.
    errorFunc = sp.erfc(v0 * sigZ / (np.sqrt(2) * Kz) + H / (np.sqrt(2) * sigZ))
    efZero = errorFunc < 1e-300
    Term3 = np.zeros(shape=(np.shape(x)), dtype=float)
    Term3[~efZero] = v0 / (vDep - vSet) * np.exp(vDep * H / Kz[~efZero] + vDep * (vDep - vSet) * sigZ[~efZero]**2 /
                                                 (2 * Kz[~efZero]**2)) * errorFunc[~efZero]
    Ndep = Q * (1 - Term1 + Term2a * Term2b - Term3)

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in Ndep.flatten())
    assert not (np.isnan(Ndep)).any()
    assert not (np.isinf(Ndep)).any()

    return Ndep


def classicGaussian(Q, U, yGrid, zGrid, H, sigY, sigZ):
    """
    Calculates the aerosol concentration in air based on the classic
    Gaussian dispersion equation (plume).

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    yGrid       numpy array of floats (n x n x n)
                The distance in y-direction in m.
    zGrid       numpy array of floats (n x n x n)
                The distance in z-direction in m.
    H           float
                The height of the emission in m.
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.

    Returns
    -------
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units.
    """

    # Calculate the concentration in location (x,y,z)
    C = Q / (2 * np.pi * U * sigY * sigZ) * \
        np.exp(-yGrid**2 / (2 * sigY**2)) * \
        (np.exp(-(zGrid - H) ** 2 / (2 * sigZ ** 2)) +
         np.exp(-(zGrid + H) ** 2 / (2 * sigZ ** 2)))

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in C.flatten())
    assert not (np.isnan(C)).any()
    assert not (np.isinf(C)).any()

    return C


def ermak(Q, U, yGrid, zGrid, H, sigY, sigZ, Kz, vSet, vDep):
    """
    Calculates the aerosol concentration in air based on Ermak (1977) solution of
    the Gaussian dispersion equation including particle settling and deposition.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    yGrid       numpy array of floats (n x n x n)
                The distance in y-direction in m.
    zGrid       numpy array of floats (n x n x n)
                The distance in z-direction in m.
    H           float
                The height of the emission in m.
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.
    Kz          numpy array of floats (n x n x n)
                The diffusivity in z-direction in m2/s.
    vSet        float
                The settling velocity of the particles in m/s.
    vDep        float
                The deposition velocity of the particles in m/s.

    Returns
    -------
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units.
    """

    # Calculate velocity variable (auxiliary), used in next calculation
    v0 = vDep - 0.5 * vSet

    # Deposition term Ermak, (1977); Stockie, (2011);
    er = np.zeros(shape=(np.shape(zGrid)), dtype=float)
    er1 = np.zeros(shape=(np.shape(zGrid)), dtype=float)
    er2 = np.exp(-(zGrid - H) ** 2 / (2 * sigZ ** 2))
    er3 = np.exp(-(zGrid + H) ** 2 / (2 * sigZ ** 2))
    er4 = np.sqrt(2 * np.pi) * v0 * sigZ / Kz
    er6 = sp.erfc((zGrid + H) / (np.sqrt(2) * sigZ) + v0 * sigZ / (np.sqrt(2) * Kz))

    # Term simplifies if er2 and er3 are zero (to avoid overflow)
    zeroEr2 = er2 < 1e-300  # boolean matrix (n x n x n)
    zeroEr3 = er3 < 1e-300  # boolean matrix (n x n x n)
    er[np.logical_and(zeroEr2, zeroEr3)] = 0.0

    # Term simplifies if er1 or er6 is zero (to avoid overflow)
    nonzeroEr = np.logical_not(np.logical_and(zeroEr2, zeroEr3))  # Exclude cases when er2 and er3 are zero
    er1[nonzeroEr] = np.exp(-vSet * (zGrid[nonzeroEr] - H) / (2 * Kz[nonzeroEr]) -
                            vSet ** 2 * sigZ[nonzeroEr] ** 2 / (8 * Kz[nonzeroEr] ** 2))
    zeroEr1 = er1 < 1e-300  # boolean matrix (n x n x n)
    zeroEr6 = er6 < 1e-300
    er[zeroEr1] = 0.0
    er[zeroEr6] = er1[zeroEr6] * (er2[zeroEr6] + er3[zeroEr6])

    # Otherwise calculate normally
    nonzeroEr = np.logical_not(np.logical_or(zeroEr1, zeroEr6))
    gamma1 = v0 * (zGrid[nonzeroEr] + H) / Kz[nonzeroEr]
    gamma2 = v0 ** 2 * sigZ[nonzeroEr] ** 2 / (2 * Kz[nonzeroEr] ** 2)
    gamma = gamma1 + gamma2
    er5 = np.exp(gamma)
    er7 = er5 * er6[nonzeroEr]
    er[nonzeroEr] = er1[nonzeroEr] * (er2[nonzeroEr] + er3[nonzeroEr] - er4[nonzeroEr] * er7)

    # Correction to er when er5 = er6 = er7 = 0
    idZe = np.argwhere(zeroEr6[:, 0, 0])  # Index of the zeros in er6
    idZe = idZe[idZe > int(zeroEr6[:, 0, 0].shape[0] / 2)]  # Discard early zero values as problem usually at end
    if not idZe.size == 0:  # If not empty array
        idFiZe = min(idZe)  # Index of the first zero in er6
        if not (np.any(er[idFiZe, :, :] == 0.0) or np.any(er[idFiZe-2, :, :] == 0.0)):  # If er value not zero
            # Take difference between values before and after the first zero
            corr = (er[idFiZe-1, :, :] / er[idFiZe, :, :])
            corr = np.tile(corr, (zeroEr6.shape[0], 1, 1))  # Replicate array to match the shape of raAdd
            # Adjust the values equal to the slope of the curve lower/higher
            slope = (er[idFiZe-1, :, :] / er[idFiZe-2, :, :])
            slope = np.tile(slope, (zeroEr6.shape[0], 1, 1))  # Replicate array to match the shape of raAdd
            # Correction happens here
            er[zeroEr6] = er[zeroEr6] * corr[zeroEr6] * slope[zeroEr6]

    # Calculate the concentration in location (x,y,z)
    C = Q / (2 * np.pi * U * sigY * sigZ) * \
        np.exp(-yGrid**2 / (2 * sigY**2)) * er

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in C.flatten())
    assert not (np.isnan(C)).any()
    assert not (np.isinf(C)).any()

    return C


def yamartino(Q, U, yGrid, zGrid, H, Hpbl, sigY, sigZ):
    """
    Calculates the aerosol concentration in air based on the Gaussian dispersion
    equation including the planetary boundary layer reflection in z-direction
    according to Yamartino, Jr. (1977) Journal of the Air Pollution Control Association.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    yGrid       numpy array of floats (n x n x n)
                The distance in y-direction in m.
    zGrid       numpy array of floats (n x n x n)
                The distance in z-direction in m.
    H           float
                The height of the emission in m.
    Hpbl        float
                The height of the planetary boundary layer in m.
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                Dispersion parameter in z-direction in m.

    Returns
    -------
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units.
    """

    # Calculate the dispersion in z-direction, including i = +/- 10 terms
    ya = np.zeros(shape=(np.shape(zGrid)), dtype=float)
    j = np.linspace(10, -10, 21)
    for i in j:
        numeratorMinus = (zGrid + 2 * i * Hpbl - H)**2
        numeratorPlus = (zGrid + 2 * i * Hpbl + H)**2
        denominator = 2 * sigZ**2
        ya = ya + np.exp(- numeratorMinus / denominator) + np.exp(- numeratorPlus / denominator)

    # Calculate the concentration in location (x,y,z)
    C = Q / (2 * np.pi * U * sigY * sigZ) * \
        np.exp(-yGrid**2 / (2 * sigY**2)) * ya

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in C.flatten())
    assert not (np.isnan(C)).any()
    assert not (np.isinf(C)).any()

    return C


def rao(Q, U, yGrid, zGrid, H, Hpbl, sigY, sigZ, Kz, vSet, vDep):
    """
    Calculates the aerosol concentration in air based on Rao (1981) eq. 17.
    Solution of the Gaussian dispersion equation including particle settling,
    deposition and boundary layer reflection.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    yGrid       numpy array of floats (n x n x n)
                The distance in y-direction in m.
    zGrid       numpy array of floats (n x n x n)
                The distance in z-direction in m.
    H           float
                The height of the emission in m.
    Hpbl        float
                The height of the planetary boundary layer in m.
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.
    Kz          numpy array of floats (n x n x n)
                The diffusivity in z-direction in m2/s.
    vSet        float
                The settling velocity of the particles in m/s.
    vDep        float
                The deposition velocity of the particles in m/s.

    Returns
    -------
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units.
    """

    # Calculate velocity variable (auxiliary)
    v0 = vDep - 0.5 * vSet

    # Dispersion in z-direction Rao, (1981) including i = +/- 10 terms
    ra = np.zeros(shape=(np.shape(zGrid)), dtype=float)
    raAdd = np.zeros(shape=(np.shape(zGrid)), dtype=float)
    j = np.linspace(10, -10, 21)
    for i in j:
        H1 = abs(H + 2 * i * Hpbl)
        eps1 = (zGrid + H1) / (np.sqrt(2) * sigZ) + v0 * sigZ / (np.sqrt(2) * Kz)
        ra2 = np.exp(-(zGrid - H1) ** 2 / (2 * sigZ ** 2))
        ra3 = np.exp(-(zGrid + H1) ** 2 / (2 * sigZ ** 2))
        ra4 = np.sqrt(2 * np.pi) * v0 * sigZ / Kz
        ra6 = sp.erfc(eps1)

        # raAdd value for each x, y, z
        # Term simplifies if both ra2 and ra3 are close to zero because then ra becomes zero
        zeroRa2 = ra2 < 1e-300  # Check which values of ra2 are close to zero
        zeroRa3 = ra3 < 1e-300  # Check which values of ra3 are close to zero
        raAdd[zeroRa2 * zeroRa3] = 0.0

        # Term also simplifies if ra6 is zero (to avoid overflow)
        zeroRa6 = ra6 < 1e-300  # Check which values of ra6 are close to zero
        zeroRa6 = zeroRa6 * ~(zeroRa2 * zeroRa3)  # Exclude cases when both ra2 and ra3 are zero
        ra1 = np.exp(-vSet * (zGrid[zeroRa6] - H1) / (2 * Kz[zeroRa6]) -
                     vSet ** 2 * sigZ[zeroRa6] ** 2 / (8 * Kz[zeroRa6] ** 2))
        raAdd[zeroRa6] = ra1 * (ra2[zeroRa6] + ra3[zeroRa6])  # Calculate a single sum term

        # Otherwise calculate normally
        nonzeroRa = ~(zeroRa2 * zeroRa3 + zeroRa6)  # Exclude cases when either ra2*ra3 or ra6 are zero
        ra1 = np.exp(-vSet * (zGrid[nonzeroRa] - H1) / (2 * Kz[nonzeroRa]) -
                     vSet ** 2 * sigZ[nonzeroRa] ** 2 / (8 * Kz[nonzeroRa] ** 2))
        ra5 = np.exp(eps1[nonzeroRa] ** 2)
        raAdd[nonzeroRa] = ra1 * (ra2[nonzeroRa] + ra3[nonzeroRa] * (1.0 - ra4[nonzeroRa] * ra5 * ra6[nonzeroRa]))

        # Correction to raAdd when ra6 = 0
        idZe = np.argwhere(zeroRa6[:, 0, 0])  # Index of the zeros in ra6
        idZe = idZe[idZe > int(zeroRa6[:, 0, 0].shape[0] / 2)]  # Discard early zero values as problem usually at end
        if not idZe.size == 0:  # If not empty array
            idFiZe = min(idZe)  # Index of the first zero in ra6
            # Don't correct if ra value really small < 1e-300 (may cause problems)
            if not (np.any(raAdd[idFiZe, :, :] < 1e-300) or np.any(raAdd[idFiZe-2, :, :] < 1e-300)):
                # Take difference between values before and after the first zero
                corr = raAdd[idFiZe-1, :, :] / raAdd[idFiZe, :, :]
                corr = np.tile(corr, (zeroRa6.shape[0], 1, 1))  # Replicate array to match the shape of raAdd
                # Adjust the values equal to the slope of the curve lower/higher
                slope = raAdd[idFiZe-1, :, :] / raAdd[idFiZe-2, :, :]
                slope = np.tile(slope, (zeroRa6.shape[0], 1, 1))  # Replicate array to match the shape of raAdd
                # Correction happens here
                raAdd[zeroRa6] = raAdd[zeroRa6] * corr[zeroRa6] * slope[zeroRa6]

        ra = ra + raAdd  # Sum the terms

        # Be sure that ra stays finite after each iteration
        assert all(isinstance(i, float) for i in ra.flatten())
        assert not (np.isnan(ra)).any()
        assert not (np.isinf(ra)).any()

    # Calculate the concentration in location (x,y,z)
    C = Q / (2 * np.pi * U * sigY * sigZ) * \
        np.exp(-yGrid**2 / (2 * sigY**2)) * ra

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in C.flatten())
    assert not (np.isnan(C)).any()
    assert not (np.isinf(C)).any()

    return C


def raoWellMixed(Q, U, yGrid, Hpbl, sigY, sigZ, Kz, vSet, vDep):
    """
    Calculates the aerosol concentration in air based on Rao (1981) eq. 20.
    Solution of the Gaussian dispersion equation including particle settling,
    deposition and boundary layer reflection for well mixed region.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    yGrid       numpy array of floats (n x n x n)
                The distance in y-direction in m.
    Hpbl        float
                The height of the planetary boundary layer in m.
    sigY        numpy array of floats (n x n x n)
                The dispersion parameter in y-direction in meters.
    sigZ        numpy array of floats (n x n x n)
                The dispersion parameter in z-direction in meters.
    Kz          numpy array of floats (n x n x n)
                The diffusivity in z-direction in m2/s.
    vSet        float
                The settling velocity of the particles in m/s.
    vDep        float
                The deposition velocity of the particles in m/s.

    Returns
    -------
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units.
    """

    # Calculate velocity variable (auxiliary)
    v0 = vDep - 0.5 * vSet

    # Initialize array
    ra = np.zeros(shape=(np.shape(yGrid)), dtype=float)
    # Dispersion in z-direction Rao, (1981) eq. 20
    ra1 = v0 / (vDep - vSet)
    ra3 = sp.erfc(v0 * sigZ / (np.sqrt(2) * Kz))
    ra4 = 0.5 * vSet / (vDep - vSet)
    ra5 = sp.erfc(vSet * sigZ / (2**(3/2) * Kz))
    # Avoid overflow in ra2 if ra3 is close to zero
    zeroRa3 = ra3 < 1e-300  # Check which values of ra3 are close to zero
    ra[zeroRa3] = 0.0  # ra simplifies to zero if ra3 close to zero
    nonzeroRa = ~zeroRa3  # Exclude cases when either ra3 is zero
    ra2 = np.exp(vDep * (vDep - vSet) * sigZ[nonzeroRa] ** 2 / (2 * Kz[nonzeroRa] ** 2))
    ra[nonzeroRa] = ra1 * ra2 * ra3[nonzeroRa] - ra4 * ra5[nonzeroRa]

    # Calculate the concentration in location (x,y,z)
    C = Q / (np.sqrt(2 * np.pi) * U * sigY * Hpbl) * \
        np.exp(-yGrid**2 / (2 * sigY**2)) * ra

    # Check that our return value is sane
    assert all(isinstance(i, float) for i in C.flatten())
    assert not (np.isnan(C)).any()
    assert not (np.isinf(C)).any()

    return C


def plumeConc(Q, U, x, y, z, H, Hpbl, dp, Temp, density, g, k, paramOption, stabClass, **kwargs):
    """
    Calculates the aerosol concentration in air based on Ermak, Yamartino and Rao solutions of
    the Gaussian dispersion equation including either particle settling and
    deposition (Ermak, 1977) or boundary layer reflection (Yamartino, 1977) or both (Rao, 1981).

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    U           float
                The wind speed in m/s.
    x           float or numpy array of floats (n x 1)
                The distance in x-direction in m.
    y           float or numpy array of floats (n x 1)
                The distance in y-direction in m.
    z           float or numpy array of floats (n x 1)
                The distance in z-direction in m.
    H           float
                The height of the emission in m.
    Hpbl        float
                The height of the planetary boundary layer in m.
    dp          float
                The mean diameter of the particles in meters.
    Temp        float
                The temperature of air in Kelvin.
    density     float
                The density of the particles in kg/m3.
    g           float
                The gravitational acceleration in m/s2.
    k           float
                The Boltzmann constant in m2*kg/(s2*K).
    paramOption string
                The parametrization to be used for calculation of the parameters, options: 'davidson' and 'klug'.
    stabClass   string
                The Pasquill atmospheric stability class, options from
                extremely unstable 'a' to stable 'f'.

    Optional    (**kwargs)
    --------
    vSet        float
                The settling velocity of the particles in m/s.
    vDep        float
                The deposition velocity of the particles in m/s.

    Returns
    -------
    Ccg         numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units
                according to the classic Gaussian equation.
    Cer         numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units
                according to Ermak (1977).
    Cya         numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units
                according to Yamartino (1977).
    Cra         numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units
                according to Rao (1981), eq. 17.
    Cwm         numpy array of floats (n x n x n)
                The aerosol concentration in air at point (x,y,z) in units
                g/m3 or #/m3 depending on the input source Q units
                according to Rao (1981), eq. 20 (well mixed region).
    """

    # Check for inputs types and values
    assert isinstance(Q, float)
    assert isinstance(U, float)
    assert all(isinstance(i, float) for i in x.flatten())
    assert all(isinstance(i, float) for i in y.flatten())
    assert all(isinstance(i, float) for i in z.flatten())
    assert isinstance(H, float)
    assert isinstance(dp, float)
    assert isinstance(Temp, float)
    assert isinstance(density, float)
    assert isinstance(g, float)
    assert isinstance(k, float)
    assert isinstance(paramOption, str)
    assert isinstance(stabClass, str)
    assert Q > 0
    assert U > 0
    assert (x > 0).all()
    assert (z >= 0).all()
    assert H >= 0
    assert dp > 0
    assert Temp > 0
    assert density > 0
    assert g > 9
    assert g <= 10
    assert k > 0
    assert Hpbl >= max(z)

    # Calculate settling velocity if not given as input parameter
    vSet = kwargs.get('vSet', settlingVelocity(dp, density, g, Temp))
    if vSet == 0.0:  # Avoid division with zero
        vSet = 1.0e-300 + 1.0e-310

    # Calculate deposition velocity if not given as input parameter
    vDep = kwargs.get('vDep', depositionVelocity(density, k, g, dp, Temp, U))
    if vDep == 0.0:  # Avoid division with zero
        vDep = 1.0e-300

    # Create location space
    xGrid, yGrid, zGrid = np.meshgrid(x, y, z, indexing='ij')

    # Calculate dispersion parameters in y- and z-directions
    sigY, sigZ = dispersionParams(xGrid, paramOption, stabClass)

    # Calculate the diffusivity in z-direction (m2/s) (Llewelyn, 1983; Stockie, 2011)
    Kz = U * sigZ**2 / (2 * xGrid)

    # Calculate the concentration in location (x,y,z)
    Ccg = classicGaussian(Q, U, yGrid, zGrid, H, sigY, sigZ)
    Cer = ermak(Q, U, yGrid, zGrid, H, sigY, sigZ, Kz, vSet, vDep)
    Cya = yamartino(Q, U, yGrid, zGrid, H, Hpbl, sigY, sigZ)
    Cra = rao(Q, U, yGrid, zGrid, H, Hpbl, sigY, sigZ, Kz, vSet, vDep)
    Cwm = raoWellMixed(Q, U, yGrid, Hpbl, sigY, sigZ, Kz, vSet, vDep)

    return Ccg, Cer, Cya, Cra, Cwm


def depositionRate(xdist, ydist, zdist, vDep, C):
    """
    Calculates the deposition rate at downwind distance x and the total deposition before distance x.

    Parameters
    ----------
    xdist       numpy array of floats (n x 1)
                The distance in x-direction in m.
    ydist       numpy array of floats (n x 1)
                The distance in y-direction in m.
    zdist       numpy array of floats (n x 1)
                The distance in z-direction in m.
    vDep        float
                The deposition velocity of the particles in m/s.
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at (x,y,z) in units g/m3 or #/m3.

    Returns
    -------
    Fdep        numpy array of floats (n x n)
                The deposition flux to ground in g/m2s or #/m2s.
    Mdep        numpy array of floats (n x n)
                The deposition rate to ground at each location (x,y) in g/s or #/s.
    MdepX       numpy array of floats (n x 1)
                The deposition rate to ground at distance x in g/s or #/s.
    MdepXcum    numpy array of floats (n x 1)
                The total deposition rate to ground before distance x in g/s or #/s.
    """

    # Check for inputs types (numpy arrays)
    assert isinstance(xdist, np.ndarray)
    assert isinstance(ydist, np.ndarray)
    assert isinstance(C, np.ndarray)

    # Check if all elements are floats
    assert isinstance(vDep, float)
    assert all(isinstance(i, float) for i in xdist.flatten())
    assert all(isinstance(i, float) for i in ydist.flatten())
    assert all(isinstance(i, float) for i in C.flatten())

    # The distance in x-direction and y-direction for each location(x, y) at ground level
    xGridSize = np.append(xdist[0], np.diff(xdist))
    yGridSize = np.append(ydist[0], np.diff(ydist))
    xGridSize2D, yGridSize2D = np.meshgrid(xGridSize, yGridSize)
    xGridSize2D = np.transpose(xGridSize2D)
    yGridSize2D = np.transpose(yGridSize2D)

    # Find the height of 2 m to calculate the deposition
    idZ = np.argmin(abs(zdist - 2))
    # Calculate the deposition flux (g/m2s) to ground (soil/surface water)
    Fdep = vDep * C[:, :, idZ]  # Ref. conc. C(z=2) = ground conc.
    # Average value of four surrounding grid points (takes the current point and previous data points)
    Fdep = ndimage.generic_filter(Fdep, np.mean, size=2)

    # Numerical mass deposition rate (g/s) in each grid point
    # Multiply the flux with the grid sizes calculated by np.diff
    Mdep = Fdep * xGridSize2D * yGridSize2D  # g/s

    # Calculate the numerical total deposition rate until x
    # Initialize arrays
    MdepX = np.zeros(shape=np.shape(xdist), dtype=float)
    MdepXcum = np.zeros(shape=np.shape(xdist), dtype=float)
    for idx, x in enumerate(xdist):
        # Integrate over dy from 0 to yMax and multiply by 2 to have -yMax to yMax (ideally -inf to inf)
        MdepX[idx] = 2.0 * np.sum(Mdep[idx, :])  # depoRate at x
        # Integrate from 0 to x and save for each x
        MdepXcum[idx] = np.sum(MdepX)  # cumulative at x

    # Check output types if all elements are floats
    assert all(isinstance(i, float) for i in Fdep.flatten())
    assert all(isinstance(i, float) for i in Mdep.flatten())
    assert all(isinstance(i, float) for i in MdepX.flatten())
    assert all(isinstance(i, float) for i in MdepXcum.flatten())

    return Fdep, Mdep, MdepX, MdepXcum


def suspensionRate(xdist, ydist, zdist, U, C):
    """
    Calculates the suspension rate at downwind distance x meaning particles suspended in air.

    Parameters
    ----------
    xdist       numpy array of floats (n x 1)
                The distance in x-direction in m.
    ydist       numpy array of floats (n x 1)
                The distance in y-direction in m.
    zdist       numpy array of floats (n x 1)
                The distance in z-direction in m.
    U           float
                The wind speed in m/s.
    C           numpy array of floats (n x n x n)
                The aerosol concentration in air at (x,y,z) in units g/m3 or #/m3.

    Returns
    -------
    Fsusp       numpy array of floats (n x n)
                The deposition flux to ground in g/m2s or #/m2s.
    Msusp       numpy array of floats (n x n)
                The deposition rate to ground at each location (x,y) in g/s or #/s.
    MsuspX      numpy array of floats (n x 1)
                The deposition rate to ground at distance x in g/s or #/s.
    """

    # Check for inputs types (numpy arrays)
    assert isinstance(xdist, np.ndarray)
    assert isinstance(ydist, np.ndarray)
    assert isinstance(zdist, np.ndarray)
    assert isinstance(C, np.ndarray)

    # Check if all elements are floats
    assert isinstance(U, float)
    assert all(isinstance(i, float) for i in xdist.flatten())
    assert all(isinstance(i, float) for i in ydist.flatten())
    assert all(isinstance(i, float) for i in zdist.flatten())
    assert all(isinstance(i, float) for i in C.flatten())

    # The distance in y-direction and z-direction for each location (y,z).
    yGridSize = np.append(ydist[0], np.diff(ydist))
    zGridSize = np.append(zdist[0], np.diff(zdist))
    yGridSize2D, zGridSize2D = np.meshgrid(yGridSize, zGridSize)
    yGridSize2D = np.transpose(yGridSize2D)
    zGridSize2D = np.transpose(zGridSize2D)

    # Suspension rate for each distance x
    Fsusp = 0.0
    Msusp = 0.0
    MsuspX = np.zeros(shape=np.shape(xdist), dtype=float)
    for idx, x in enumerate(xdist):
        # Calculate the particle flux (g/m2s) in wind direction
        Fsusp = U * C[idx, :, :]
        # Average value of four surrounding grid points (takes the current point and previous data points)
        Fsusp = ndimage.generic_filter(Fsusp, np.mean, size=2)
        # Suspension rate (g/s) in each grid point
        # Multiply the flux with the grid sizes calculated by np.diff
        Msusp = Fsusp * yGridSize2D * zGridSize2D  # g/s
        # Integrate over dy from 0 to yMax and multiply by 2 to have -yMax to yMax (ideally -inf to inf)
        # Also integrate from 0 to z and save for each x
        MsuspX[idx] = 2.0 * np.sum(Msusp[:, :])  # suspension rate at x

    # Check output types if all elements are floats
    assert all(isinstance(i, float) for i in Fsusp.flatten())
    assert all(isinstance(i, float) for i in Msusp.flatten())
    assert all(isinstance(i, float) for i in MsuspX.flatten())

    return Fsusp, Msusp, MsuspX


def massBalance(Q, xdist, MdepXcum, MsuspX, Cuncorr):
    """
    Calculates concentration corrected with mass balance.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    xdist       numpy array of floats (n x 1)
                The distance in x-direction in m.
    MdepXcum    numpy array of floats (n x 1)
                The total deposition rate as function of x.
    MsuspX      numpy array of floats (n x 1)
                The suspension rate as function of x.
    Cuncorr     numpy array of floats (n x n x n)
                The uncorrected aerosol concentration in air at ground level (x,y,z) in units
                g/m3 or #/m3.

    Returns
    -------
    Ccorr       numpy array of floats (n x n x n)
                The corrected aerosol concentration in air at ground level (x,y,z) in units
                g/m3 or #/m3.
    """

    # Check for inputs types (numpy arrays)
    assert isinstance(Cuncorr, np.ndarray)

    # Check if all elements are floats
    assert isinstance(Q, float)
    assert all(isinstance(i, float) for i in Cuncorr.flatten())

    # Calculate corrected concentration field by forcing mass balance (Q = MdepXcum + MsuspX)
    Ccorr = np.zeros(shape=np.shape(Cuncorr), dtype=float)
    for idx, x in enumerate(xdist):
        corr = Q / (MsuspX[idx] + MdepXcum[idx])
        Ccorr[idx, :, :] = corr * Cuncorr[idx, :, :]

        assert not np.any(Ccorr < 0.0)  # If any value in Ccorr is negative, give assertion error

    # Check output types if all elements are floats
    assert all(isinstance(i, float) for i in Ccorr.flatten())

    return Ccorr


def massBalanceIteration(Q, xdist, ydist, zdist, vDep, U, Cuncorr):
    """
    Calculates concentration corrected with mass balance, iterate until a tolerance of 1 percent is reached.

    Parameters
    ----------
    Q           float
                The source term from the continuous source in g/s or #/s.
    xdist       numpy array of floats (n x 1)
                The distance in x-direction in m.
    ydist       numpy array of floats (n x 1)
                The distance in y-direction in m.
    zdist       numpy array of floats (n x 1)
                The distance in z-direction in m.
    vDep        float
                The deposition velocity of the particles in m/s.
    U           float
                The wind speed in m/s.
    Cuncorr     numpy array of floats (n x n x n)
                The uncorrected aerosol concentration in air at ground level (x,y,z) in units
                g/m3 or #/m3.

    Returns
    -------
    Ccorr       numpy array of floats (n x n x n)
                The corrected aerosol concentration in air at ground level (x,y,z) in units
                g/m3 or #/m3.
    numIter     int
                The number of iterations needed for tolerance of 0.1 percent
    tolerance   float
                The maximum relative difference from the mass balance (fraction of 1)
    MdepXcumC   numpy array of floats (20 x n)
                The corrected total deposition rate as function of x for each iteration.
    MsuspXc     numpy array of floats (20 x n)
                The corrected suspension rate as function of x for each iteration.
    """

    # Initiate variables
    Ccorr = Cuncorr  # Initial concentration
    _, _, _, MdepXcum = depositionRate(xdist, ydist, zdist, vDep, Cuncorr)  # Vertical total flux due to deposition
    _, _, MsuspX = suspensionRate(xdist, ydist, zdist, U, Cuncorr)  # Horizontal total flux due to wind
    numIter = 0  # Number of current iteration
    tolerance = 1.0
    iterMax = 200
    MdepXcumC = np.zeros(shape=(iterMax, np.shape(xdist)[0]), dtype=float)
    MsuspXc = np.zeros(shape=(iterMax, np.shape(xdist)[0]), dtype=float)

    # Calculate corrected concentration field, stop when tolerance of 1 percent reached.
    while tolerance > 0.01:
        # Corrected concentration
        Ccorr = massBalance(Q, xdist, MdepXcum, MsuspX, Ccorr)
        # Total deposition and suspension for the corrected concentration
        _, _, _, MdepXcum = depositionRate(xdist, ydist, zdist, vDep, Ccorr)
        _, _, MsuspX = suspensionRate(xdist, ydist, zdist, U, Ccorr)
        # Save deposition and suspension rates to arrays for each iteration
        MdepXcumC[numIter, :] = MdepXcum
        MsuspXc[numIter, :] = MsuspX
        # Calculate mass balance
        totMass = (MdepXcum + MsuspX) / Q
        # Calculate the maximal difference from mass balance
        toleranceMin = abs(1 - min(abs(totMass)))
        toleranceMax = abs(1 - max(abs(totMass)))
        tolerance = max(toleranceMin, toleranceMax)
        # Count number of iterations
        numIter = numIter + 1
        # Exit the while loop after max iterations even if tolerance is not reached
        if numIter == iterMax:
            break
    print('Number of iterations', numIter, 'tolerance', "{:.3f}".format(tolerance))

    # Check output types if all elements are floats
    assert isinstance(numIter, int)
    assert isinstance(tolerance, float)
    assert all(isinstance(i, float) for i in Ccorr.flatten())
    assert all(isinstance(i, float) for i in MdepXcumC.flatten())
    assert all(isinstance(i, float) for i in MsuspXc.flatten())

    return Ccorr, numIter, tolerance, MdepXcumC, MsuspXc
