ADiDeNano - Documentation
=========================

Documentation and examples of use are reported in a scientific publication: Poikkimäki et al. (2022), Local Scale Exposure and Fate of Engineered Nanomaterials, Toxics.